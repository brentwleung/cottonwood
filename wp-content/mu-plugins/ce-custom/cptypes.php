<?php
/*
Plugin Name: Custom Post Types
Description: Custom post types and related items for website functionality.
Version: 1.0
Author: Brent Leung
Author URI: http://www.brentleung.com/
*/


$cptTaxThemeSlug = 'mining';

// Register Event Post Type
function cpt_event() {

    global $cptTaxThemeSlug;

    $labels = array(
        'name'                  => _x( 'Events', 'Post Type General Name', $cptTaxThemeSlug ),
        'singular_name'         => _x( 'Event', 'Post Type Singular Name', $cptTaxThemeSlug ),
        'menu_name'             => __( 'Events', $cptTaxThemeSlug ),
        'name_admin_bar'        => __( 'Events', $cptTaxThemeSlug ),
        'archives'              => __( 'Event Archives', $cptTaxThemeSlug ),
        'parent_item_colon'     => __( 'Event Partner:', $cptTaxThemeSlug ),
        'all_items'             => __( 'All Events', $cptTaxThemeSlug ),
        'add_new_item'          => __( 'Add New Event', $cptTaxThemeSlug ),
        'add_new'               => __( 'Add New', $cptTaxThemeSlug ),
        'new_item'              => __( 'New Event', $cptTaxThemeSlug ),
        'edit_item'             => __( 'Edit Event', $cptTaxThemeSlug ),
        'update_item'           => __( 'Update Event', $cptTaxThemeSlug ),
        'view_item'             => __( 'View Event', $cptTaxThemeSlug ),
        'search_items'          => __( 'Search Event', $cptTaxThemeSlug ),
        'not_found'             => __( 'Not found', $cptTaxThemeSlug ),
        'not_found_in_trash'    => __( 'Not found in Trash', $cptTaxThemeSlug ),
        'featured_image'        => __( 'Featured Image', $cptTaxThemeSlug ),
        'set_featured_image'    => __( 'Set featured image', $cptTaxThemeSlug ),
        'remove_featured_image' => __( 'Remove featured image', $cptTaxThemeSlug ),
        'use_featured_image'    => __( 'Use as featured image', $cptTaxThemeSlug ),
        'insert_into_item'      => __( 'Insert into item', $cptTaxThemeSlug ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', $cptTaxThemeSlug ),
        'items_list'            => __( 'Items list', $cptTaxThemeSlug ),
        'items_list_navigation' => __( 'Items list navigation', $cptTaxThemeSlug ),
        'filter_items_list'     => __( 'Filter items list', $cptTaxThemeSlug ),
    );
    $rewrite = array(
        'slug'                  => 'our-events',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => false,
    );

    $args = array(
        'label'                 => __( 'Event', $cptTaxThemeSlug ),
        'description'           => __( 'Mining for Miracles events', $cptTaxThemeSlug ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'revisions', 'page-attributes', 'thumbnail', 'author', 'editor' ),
        'taxonomies'            => array( 'event_type' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-tickets-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'event', $args );

}
add_action( 'init', 'cpt_event', 0 );


// ------------------------------------------------------------------------------------------------------------- //


// Register Team Member Post Type
function cpt_team() {

    global $cptTaxThemeSlug;

    $labels = array(
        'name'                  => _x( 'Team Members', 'Post Type General Name', $cptTaxThemeSlug ),
        'singular_name'         => _x( 'Team Member', 'Post Type Singular Name', $cptTaxThemeSlug ),
        'menu_name'             => __( 'Team Members', $cptTaxThemeSlug ),
        'name_admin_bar'        => __( 'Team Members', $cptTaxThemeSlug ),
        'archives'              => __( 'Item Archives', $cptTaxThemeSlug ),
        'parent_item_colon'     => __( 'Parent Team Member:', $cptTaxThemeSlug ),
        'all_items'             => __( 'All Team Members', $cptTaxThemeSlug ),
        'add_new_item'          => __( 'Add New Team Member', $cptTaxThemeSlug ),
        'add_new'               => __( 'Add New', $cptTaxThemeSlug ),
        'new_item'              => __( 'New Team Member', $cptTaxThemeSlug ),
        'edit_item'             => __( 'Edit Team Member', $cptTaxThemeSlug ),
        'update_item'           => __( 'Update Team Member', $cptTaxThemeSlug ),
        'view_item'             => __( 'View Team Member', $cptTaxThemeSlug ),
        'search_items'          => __( 'Search Team Members', $cptTaxThemeSlug ),
        'not_found'             => __( 'Not found', $cptTaxThemeSlug ),
        'not_found_in_trash'    => __( 'Not found in Trash', $cptTaxThemeSlug ),
        'featured_image'        => __( 'Featured Image', $cptTaxThemeSlug ),
        'set_featured_image'    => __( 'Set featured image', $cptTaxThemeSlug ),
        'remove_featured_image' => __( 'Remove featured image', $cptTaxThemeSlug ),
        'use_featured_image'    => __( 'Use as featured image', $cptTaxThemeSlug ),
        'insert_into_item'      => __( 'Insert into item', $cptTaxThemeSlug ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', $cptTaxThemeSlug ),
        'items_list'            => __( 'Items list', $cptTaxThemeSlug ),
        'items_list_navigation' => __( 'Items list navigation', $cptTaxThemeSlug ),
        'filter_items_list'     => __( 'Filter items list', $cptTaxThemeSlug ),
    );
    $rewrite = array(
        'slug'                  => 'our-team',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => false,
    );
    $args = array(
        'label'                 => __( 'Team Member', $cptTaxThemeSlug ),
        'description'           => __( 'mining CEO team members', $cptTaxThemeSlug ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'revisions', 'page-attributes', 'thumbnail', 'author', 'editor' ),
        'taxonomies'            => array( 'team_type' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-groups',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'team', $args );

}
add_action( 'init', 'cpt_team', 0 );


// ------------------------------------------------------------------------------------------------------------- //



// Register Resource Post Type
function cpt_resource() {

    global $cptTaxThemeSlug;

    $labels = array(
        'name'                  => _x( 'Resources', 'Post Type General Name', $cptTaxThemeSlug ),
        'singular_name'         => _x( 'Resource', 'Post Type Singular Name', $cptTaxThemeSlug ),
        'menu_name'             => __( 'Resources', $cptTaxThemeSlug ),
        'name_admin_bar'        => __( 'Resources', $cptTaxThemeSlug ),
        'archives'              => __( 'Item Archives', $cptTaxThemeSlug ),
        'parent_item_colon'     => __( 'Parent Resource:', $cptTaxThemeSlug ),
        'all_items'             => __( 'All Resources', $cptTaxThemeSlug ),
        'add_new_item'          => __( 'Add New Resource', $cptTaxThemeSlug ),
        'add_new'               => __( 'Add New', $cptTaxThemeSlug ),
        'new_item'              => __( 'New Resource', $cptTaxThemeSlug ),
        'edit_item'             => __( 'Edit Resource', $cptTaxThemeSlug ),
        'update_item'           => __( 'Update Resource', $cptTaxThemeSlug ),
        'view_item'             => __( 'View Resource', $cptTaxThemeSlug ),
        'search_items'          => __( 'Search Resources', $cptTaxThemeSlug ),
        'not_found'             => __( 'Not found', $cptTaxThemeSlug ),
        'not_found_in_trash'    => __( 'Not found in Trash', $cptTaxThemeSlug ),
        'featured_image'        => __( 'Featured Image', $cptTaxThemeSlug ),
        'set_featured_image'    => __( 'Set featured image', $cptTaxThemeSlug ),
        'remove_featured_image' => __( 'Remove featured image', $cptTaxThemeSlug ),
        'use_featured_image'    => __( 'Use as featured image', $cptTaxThemeSlug ),
        'insert_into_item'      => __( 'Insert into item', $cptTaxThemeSlug ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', $cptTaxThemeSlug ),
        'items_list'            => __( 'Items list', $cptTaxThemeSlug ),
        'items_list_navigation' => __( 'Items list navigation', $cptTaxThemeSlug ),
        'filter_items_list'     => __( 'Filter items list', $cptTaxThemeSlug ),
    );
    $rewrite = array(
        'slug'                  => 'our-resources',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => false,
    );
    $args = array(
        'label'                 => __( 'Resource', $cptTaxThemeSlug ),
        'description'           => __( 'Resources', $cptTaxThemeSlug ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'revisions', 'page-attributes', 'thumbnail', 'author', 'editor' ),
        'taxonomies'            => array( 'resource_type' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-analytics',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'resource', $args );

}
add_action( 'init', 'cpt_resource', 0 );




// Register Resource Post Type
function cpt_covid() {

    global $cptTaxThemeSlug;

    $labels = array(
        'name'                  => _x( 'Covid-19', 'Post Type General Name', $cptTaxThemeSlug ),
        'singular_name'         => _x( 'Covid-19', 'Post Type Singular Name', $cptTaxThemeSlug ),
        'menu_name'             => __( 'Covid-19', $cptTaxThemeSlug ),
        'name_admin_bar'        => __( 'Covid-19', $cptTaxThemeSlug ),
        'archives'              => __( 'Item Archives', $cptTaxThemeSlug ),
        'parent_item_colon'     => __( 'Parent Covid-19:', $cptTaxThemeSlug ),
        'all_items'             => __( 'All Covid-19', $cptTaxThemeSlug ),
        'add_new_item'          => __( 'Add New Covid-19', $cptTaxThemeSlug ),
        'add_new'               => __( 'Add New', $cptTaxThemeSlug ),
        'new_item'              => __( 'New Covid-19', $cptTaxThemeSlug ),
        'edit_item'             => __( 'Edit Covid-19', $cptTaxThemeSlug ),
        'update_item'           => __( 'Update Covid-19', $cptTaxThemeSlug ),
        'view_item'             => __( 'View Covid-19', $cptTaxThemeSlug ),
        'search_items'          => __( 'Search Covid-19', $cptTaxThemeSlug ),
        'not_found'             => __( 'Not found', $cptTaxThemeSlug ),
        'not_found_in_trash'    => __( 'Not found in Trash', $cptTaxThemeSlug ),
        'featured_image'        => __( 'Featured Image', $cptTaxThemeSlug ),
        'set_featured_image'    => __( 'Set featured image', $cptTaxThemeSlug ),
        'remove_featured_image' => __( 'Remove featured image', $cptTaxThemeSlug ),
        'use_featured_image'    => __( 'Use as featured image', $cptTaxThemeSlug ),
        'insert_into_item'      => __( 'Insert into item', $cptTaxThemeSlug ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', $cptTaxThemeSlug ),
        'items_list'            => __( 'Items list', $cptTaxThemeSlug ),
        'items_list_navigation' => __( 'Items list navigation', $cptTaxThemeSlug ),
        'filter_items_list'     => __( 'Filter items list', $cptTaxThemeSlug ),
    );
    $rewrite = array(
        'slug'                  => 'our-covid-19-updates',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => false,
    );
    $args = array(
        'label'                 => __( 'Covid-19', $cptTaxThemeSlug ),
        'description'           => __( 'Covid-19', $cptTaxThemeSlug ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'revisions', 'page-attributes', 'thumbnail', 'author', 'editor' ),
        //'taxonomies'            => array( 'resource_type' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-megaphone',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'covid', $args );

}
add_action( 'init', 'cpt_covid', 0 );

// ------------------------------------------------------------------------------------------------------------- //
// ====================================================TAXONOMIES=================================================
// ------------------------------------------------------------------------------------------------------------- //



// Register Event Type Taxonomy
function tax_event() {

    global $cptTaxThemeSlug;

    $labels = array(
        'name'                       => _x( 'Event Types', 'Taxonomy General Name', $cptTaxThemeSlug ),
        'singular_name'              => _x( 'Event Type', 'Taxonomy Singular Name', $cptTaxThemeSlug ),
        'menu_name'                  => __( 'Event Types', $cptTaxThemeSlug ),
        'all_items'                  => __( 'All Event Types', $cptTaxThemeSlug ),
        'parent_item'                => __( 'Parent Event Type', $cptTaxThemeSlug ),
        'parent_item_colon'          => __( 'Parent Event Type:', $cptTaxThemeSlug ),
        'new_item_name'              => __( 'New Event Type Name', $cptTaxThemeSlug ),
        'add_new_item'               => __( 'Add New Event Type', $cptTaxThemeSlug ),
        'edit_item'                  => __( 'Edit Event Type', $cptTaxThemeSlug ),
        'update_item'                => __( 'Update Event Type', $cptTaxThemeSlug ),
        'view_item'                  => __( 'View Event Type', $cptTaxThemeSlug ),
        'separate_items_with_commas' => __( 'Separate regions with commas', $cptTaxThemeSlug ),
        'add_or_remove_items'        => __( 'Add or remove regions', $cptTaxThemeSlug ),
        'choose_from_most_used'      => __( 'Choose from the most used', $cptTaxThemeSlug ),
        'popular_items'              => __( 'Popular Event Types', $cptTaxThemeSlug ),
        'search_items'               => __( 'Search Event Types', $cptTaxThemeSlug ),
        'not_found'                  => __( 'Not Found', $cptTaxThemeSlug ),
        'no_terms'                   => __( 'No items', $cptTaxThemeSlug ),
        'items_list'                 => __( 'Items list', $cptTaxThemeSlug ),
        'items_list_navigation'      => __( 'Items list navigation', $cptTaxThemeSlug ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => false,
    );
    register_taxonomy( 'event_type', array( 'event' ), $args );

}
add_action( 'init', 'tax_event', 0 );


// ------------------------------------------------------------------------------------------------------------- //


// Register Team Type Taxonomy
function tax_team() {

    global $cptTaxThemeSlug;

    $labels = array(
        'name'                       => _x( 'Team Types', 'Taxonomy General Name', $cptTaxThemeSlug ),
        'singular_name'              => _x( 'Team Type', 'Taxonomy Singular Name', $cptTaxThemeSlug ),
        'menu_name'                  => __( 'Team Types', $cptTaxThemeSlug ),
        'all_items'                  => __( 'All Team Types', $cptTaxThemeSlug ),
        'parent_item'                => __( 'Parent Team Type', $cptTaxThemeSlug ),
        'parent_item_colon'          => __( 'Parent Team Type:', $cptTaxThemeSlug ),
        'new_item_name'              => __( 'New Team Type', $cptTaxThemeSlug ),
        'add_new_item'               => __( 'Add New Team Type', $cptTaxThemeSlug ),
        'edit_item'                  => __( 'Edit Team Type', $cptTaxThemeSlug ),
        'update_item'                => __( 'Update Team Type', $cptTaxThemeSlug ),
        'view_item'                  => __( 'View Team Type', $cptTaxThemeSlug ),
        'separate_items_with_commas' => __( 'Separate styles with commas', $cptTaxThemeSlug ),
        'add_or_remove_items'        => __( 'Add or remove styles', $cptTaxThemeSlug ),
        'choose_from_most_used'      => __( 'Choose from the most used', $cptTaxThemeSlug ),
        'popular_items'              => __( 'Popular Team Types', $cptTaxThemeSlug ),
        'search_items'               => __( 'Search Team Types', $cptTaxThemeSlug ),
        'not_found'                  => __( 'Not Found', $cptTaxThemeSlug ),
        'no_terms'                   => __( 'No items', $cptTaxThemeSlug ),
        'items_list'                 => __( 'Items list', $cptTaxThemeSlug ),
        'items_list_navigation'      => __( 'Items list navigation', $cptTaxThemeSlug ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => false,
    );
    register_taxonomy( 'team_type', array( 'team' ), $args );

}
add_action( 'init', 'tax_team', 0 );


// ------------------------------------------------------------------------------------------------------------- //


// Register Resources Type Taxonomy
function tax_resources() {

    global $cptTaxThemeSlug;

    $labels = array(
        'name'                       => _x( 'Resource Types', 'Taxonomy General Name', $cptTaxThemeSlug ),
        'singular_name'              => _x( 'Resource Type', 'Taxonomy Singular Name', $cptTaxThemeSlug ),
        'menu_name'                  => __( 'Resource Types', $cptTaxThemeSlug ),
        'all_items'                  => __( 'All Resource Types', $cptTaxThemeSlug ),
        'parent_item'                => __( 'Parent Resource Type', $cptTaxThemeSlug ),
        'parent_item_colon'          => __( 'Parent Resource Type:', $cptTaxThemeSlug ),
        'new_item_name'              => __( 'New Resource Type', $cptTaxThemeSlug ),
        'add_new_item'               => __( 'Add New Resource Type', $cptTaxThemeSlug ),
        'edit_item'                  => __( 'Edit Resource Type', $cptTaxThemeSlug ),
        'update_item'                => __( 'Update Resource Type', $cptTaxThemeSlug ),
        'view_item'                  => __( 'View Resource Type', $cptTaxThemeSlug ),
        'separate_items_with_commas' => __( 'Separate styles with commas', $cptTaxThemeSlug ),
        'add_or_remove_items'        => __( 'Add or remove styles', $cptTaxThemeSlug ),
        'choose_from_most_used'      => __( 'Choose from the most used', $cptTaxThemeSlug ),
        'popular_items'              => __( 'Popular Resource Types', $cptTaxThemeSlug ),
        'search_items'               => __( 'Search Resource Types', $cptTaxThemeSlug ),
        'not_found'                  => __( 'Not Found', $cptTaxThemeSlug ),
        'no_terms'                   => __( 'No items', $cptTaxThemeSlug ),
        'items_list'                 => __( 'Items list', $cptTaxThemeSlug ),
        'items_list_navigation'      => __( 'Items list navigation', $cptTaxThemeSlug ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => false,
    );
    register_taxonomy( 'resource_type', array( 'resource' ), $args );

}
add_action( 'init', 'tax_resources', 0 );


?>
