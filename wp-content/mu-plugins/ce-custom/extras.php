<?php
/*
Plugin Name: Website MU functionality
Description: Extra custom functionality.
Version: 1.0
Author: Brent Leung
Author URI: http://brentleung.com/
*/

function auto_copyright($year = 'auto'){
	if(intval($year) == 'auto'){ $year = date('Y'); }
	if(intval($year) == date('Y')){ echo intval($year); }
	if(intval($year) < date('Y')){ echo intval($year) . ' - ' . date('Y'); }
	if(intval($year) > date('Y')){ echo date('Y'); }
}


add_filter( 'wpseo_metabox_prio', function() { return 'low';});


// Add img-responsive class to images inserted in post content
function add_image_responsive_class($content) {
    global $post;
    $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
    $replacement = '<img$1class="$2 img-fluid"$3>';
    $content = preg_replace($pattern, $replacement, $content);
    return $content;
}
add_filter('the_content', 'add_image_responsive_class');


// get slug
function the_slug() {
    $post_data = get_post($post->ID, ARRAY_A);
    $slug = $post_data['post_name'];
    return $slug;
}

// Function to more easily conditionally select blog-related pages
function is_blog() {
    if (is_home() || is_singular('post') || is_post_type_archive('post') || is_category() || is_tag() || is_author() || is_date())
        return true;
    else return false;
}
add_filter( 'body_class','my_body_classes' );
function my_body_classes( $classes ) {
    if ( is_blog() ) {
        $classes[] = 'blog';
    }
    return $classes;
}

// enhanced pagination
function pagination($pages = '', $range = 4)
{
	$showitems = ($range * 2)+1;

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == '')
	{
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages)
		{
			$pages = 1;
		}
	}

	if(1 != $pages)
	{
		echo "<strong>Page ".$paged." of ".$pages."</strong>";
	}
}


