<?php
/*
  Plugin Name: Website MU functionality
  Description: Site-specific functionality
  Author: Brent Leung
  Version: 1.0
 */

require(dirname(__FILE__)."/ce-custom/cptypes.php");
require(dirname(__FILE__)."/ce-custom/extras.php");
