<?php
    $size_xs   = 'banner-xs';
    $size_sm   = 'banner-sm';
    $size_md   = 'banner-md';
    $size_lg   = 'banner-lg';
    $size_xl   = 'full';
    $term = get_queried_object();
?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="format-detection" content="telephone=no">
  <?php wp_head(); ?>
  <?php
    if( is_404() ):
        $bnr_id = get_field('page_banner_img', 'option');
    elseif(is_category()) :
        $bnr_id = get_field('category_image', $term);
    else:
        $bnr_id = get_field('page_banner_img', get_queried_object_id());
    endif;

    if ( !empty($bnr_id) ) :
        $banner_xs  = wp_get_attachment_image_src( $bnr_id, $size_xs );
        $banner_sm  = wp_get_attachment_image_src( $bnr_id, $size_sm );
        $banner_md  = wp_get_attachment_image_src( $bnr_id, $size_md );
        $banner_lg  = wp_get_attachment_image_src( $bnr_id, $size_lg );
        $banner_xl  = wp_get_attachment_image_src( $bnr_id, $size_xl );
    endif; ?>

    <style>
        .page-banner {background-image:url(<?php echo $banner_xs['0']; ?>);}
        @media (min-width: 768px) {.page-banner { background-image:url(<?php echo $banner_sm['0']; ?>);}}
        @media (min-width: 992px) {.page-banner {background-image:url(<?php echo $banner_md['0']; ?>);}}
        @media (min-width: 1200px) {.page-banner { background-image:url(<?php echo $banner_lg['0']; ?>);}}
        @media (min-width: 1439px) {.page-banner { background-image:url(<?php echo $banner_xl['0']; ?>);}}
    </style>

    <?php if (is_page_template(array('template-suite-choices.php', 'template-amenities.php'))) :

        $featImg_id = get_field('feature_image');

        if ( !empty($featImg_id) ) :
            $feat_xs  = wp_get_attachment_image_src( $featImg_id, $size_xs );
            $feat_sm  = wp_get_attachment_image_src( $featImg_id, $size_sm );
            $feat_md  = wp_get_attachment_image_src( $featImg_id, $size_md );
            $feat_lg  = wp_get_attachment_image_src( $featImg_id, $size_lg );
            $feat_xl  = wp_get_attachment_image_src( $featImg_id, $size_xl );
        endif; ?>

        <style>
            .feature-image {background-image:url(<?php echo $feat_xs['0']; ?>);}
            @media (min-width: 768px) {.feature-image { background-image:url(<?php echo $feat_sm['0']; ?>);}}
            @media (min-width: 992px) {.feature-image {background-image:url(<?php echo $feat_md['0']; ?>);}}
            @media (min-width: 1200px) {.feature-image { background-image:url(<?php echo $feat_lg['0']; ?>);}}
            @media (min-width: 1439px) {.feature-image { background-image:url(<?php echo $feat_xl['0']; ?>);}}
        </style>

    <?php endif ?>

    <?php if (!is_front_page() && !is_404()) :

        $preFooterCTA = get_field('pre_footer_bg_img', 'option');

        if ( !empty($preFooterCTA) ) :
            $ctaXS  = wp_get_attachment_image_src( $preFooterCTA, $size_xs );
            $ctaSM  = wp_get_attachment_image_src( $preFooterCTA, $size_sm );
            $ctaMD  = wp_get_attachment_image_src( $preFooterCTA, $size_md );
            $ctaLG  = wp_get_attachment_image_src( $preFooterCTA, $size_lg );
            $ctaXL  = wp_get_attachment_image_src( $preFooterCTA, $size_xl );
        endif; ?>

        <style>
            .pre-footer-bg-img {background-image:url(<?php echo $ctaXS['0']; ?>);}
            @media (min-width: 768px) {.pre-footer-bg-img { background-image:url(<?php echo $ctaSM['0']; ?>);}}
            @media (min-width: 992px) {.pre-footer-bg-img {background-image:url(<?php echo $ctaMD['0']; ?>);}}
            @media (min-width: 1200px) {.pre-footer-bg-img { background-image:url(<?php echo $ctaLG['0']; ?>);}}
            @media (min-width: 1439px) {.pre-footer-bg-img { background-image:url(<?php echo $ctaXL['0']; ?>);}}
        </style>

    <?php endif ?>
</head>
