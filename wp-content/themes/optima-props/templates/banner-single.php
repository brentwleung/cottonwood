<div class="single-banner d-flex container-fluid">
    <div class="row row-center">
        <div class="col-11 col-sm-12 col-md-10 col-lg-9 col-xl-8">
            <h2 class="main-heading has-underline">
                <?php the_field( 'single_banner_heading', 'option' ); ?>
            </h2>
        </div>
    </div>
</div>
