<?php
    $siteLogo = get_field('site_logo', 'option');
    $url = $siteLogo['url'];
    $alt = $siteLogo['alt'];
?>

<header class="header flex-center justify-content-end">
    <div class="container-fluid">
        <div class="row">
            <div class="col-9 col-md-5 col-xl-4 offset-xl-1 brand-col">
                <a class="is-block is-on-top brand-link" title="Company logo" href="<?= esc_url(home_url('/')); ?>">
                    <img class="site-logo" src="<?php echo $url; ?>" <?php if($alt) { echo 'alt="' . $alt .'"'; } ?>>
                </a>
            </div>
            <div class="right-col flex-center col-xl-7 d-none d-xl-flex justify-content-xl-end">
                <form class="global-search-form is-relative" role="search" method="GET" action="<?php echo home_url('/'); ?>">
                    <label for="global-search"><span></span></label>
                    <input id="global-search" class="search-bar" type="serach" placeholder='SEARCH' name="s" value="<?php echo get_search_query(); ?>">
                </form>
                <?php while ( have_rows('header_btn', 'option') ) : the_row(); ?>
                    <?php get_template_part('templates/button'); ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <button class="gn--trigger">
        <span class="burger"></span>
        <span class="burger"></span>
        <span class="burger"></span>
    </button>
</header>
<div class="gn-wrap">
    <nav class="gn" role="navigation">
        <?php if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'gn--items', 'container' => '']);
        endif; ?>
        <div class="mobile-search d-xl-none">
            <form class="global-search-form is-relative" role="search" method="GET" action="<?php echo home_url('/'); ?>">
                <label for="global-search"><span></span></label>
                <input id="global-search" class="search-bar" type="serach" placeholder='SEARCH' name="s" value="<?php echo get_search_query(); ?>">
            </form>
            <?php while ( have_rows('header_btn', 'option') ) : the_row(); ?>
                <?php get_template_part('templates/button'); ?>
            <?php endwhile; ?>
        </div>
    </nav>
</div>
