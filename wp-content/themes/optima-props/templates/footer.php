<footer class="footer">
    <div class="container-fluid">
		<?php $facebookLink = get_field('facebook_link', 'option'); ?>
        <?php if( $facebookLink) : ?>
            <div class="row social-row row-center">
                <div class="col-11 col-xl-10 social-col">
                   <!-- <a href="<?php echo $facebookLink ?>" class="d-inline-block is-on-top icon" target="_blank" rel="noopener" title="Link to our Facebook page">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 50 50" style=" fill:#005aa0;">
                            <path d="M32,11h5c0.552,0,1-0.448,1-1V3.263c0-0.524-0.403-0.96-0.925-0.997C35.484,2.153,32.376,2,30.141,2C24,2,20,5.68,20,12.368 V19h-7c-0.552,0-1,0.448-1,1v7c0,0.552,0.448,1,1,1h7v19c0,0.552,0.448,1,1,1h7c0.552,0,1-0.448,1-1V28h7.222 c0.51,0,0.938-0.383,0.994-0.89l0.778-7C38.06,19.518,37.596,19,37,19h-8v-5C29,12.343,30.343,11,32,11z">
                            </path>
                        </svg>
                    </a>-->
                </div>
            </div>
        <?php endif; ?>
        <?php if (get_field('social_media_links', 'option')): ?>
        	<div class="social_media col-8">
        	<style>
	        	.social_media {display: flex; flex-wrap: wrap; padding-bottom: 40px; justify-content: flex-end; margin: 0 auto;
	        	}
	        	.social_media .icon {flex-basis: 30px; margin-left: 10px;}
	        	.social_media .icon:hover {opacity: 0.7}
        	</style>
        		<?php while( have_rows('social_media_links', 'option') ) : the_row(); ?>
			        <div class="icon"><a href="<?php the_sub_field('link', 'option'); ?>" target="_blank"><img src="<?php the_sub_field('icon', 'option'); ?>" alt="Social Media Icon" /></a></div>
			    <?php endwhile; ?>
        	</div>
<?php endif; ?>
        <div class="row row-center nav-row">
            <div class="col-11 col-sm-7 col-md-3 col-lg-3 main-ftr-logo">
                <?php if(the_field('main_footer_logo', 'option')) : ?>
                    <img class="img-fluid" src="<?php the_field('main_footer_logo', 'option'); ?>" alt="Cottonwood Logo">
                <?php endif; ?>
            </div>
            <div class="col-11 col-md-9 col-lg-9 main-ftr-nav">
                <?php if (has_nav_menu('footer_navigation')) :
                    wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'ftr--items', 'container' => '']);
                endif; ?>
            </div>
        </div>
        <div class="row row-center align-items-md-center legal-row">
            <div class="col-11 col-md-4 col-lg-5 col-xl-4 order-3 order-md-0 main-ftr-copyright">
                <p class="copyright">&copy; <?php echo date('Y '); the_field( 'copyright_name', 'option' ); ?> <span class="design-by d-block d-lg-inline-block">Designed by <strong><a href="https://www.redrocketcreative.com/" target="_blank" rel="noopener">Red Rocket Creative Strategies</strong></span></a></p>
            </div>
            <div class="col-11 col-md-5 col-lg-4 col-xl-5 order-1 order-md-2 footer-brand-img">
                <a href="<?php the_field('visit_optima_living_link', 'option') ?>" target="_blank" rel="noopener">
                    <img src="<?php the_field('optima_living_logo', 'option'); ?>" alt="Cottonwood Logo">
                </a>
            </div>
            <div class="col-11 col-md-3 optima-link order-md-7">
                <p>
                    <a href="<?php the_field('visit_optima_living_link', 'option') ?>" target="_blank" rel="noopener">
                        <?php the_field( 'visit_optima_living_link_text', 'option' ); ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</footer>
