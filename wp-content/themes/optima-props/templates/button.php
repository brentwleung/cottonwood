<?php

/*==================================================
    Button
==================================================*/

    $buttonText = get_sub_field( 'button_text' );
    $buttonClass = get_sub_field( 'button_style' );

    if (get_sub_field('button_type') == 'download') {
       $buttonLink =  get_sub_field('download_file');
       $buttonAttr  = 'target="_blank" rel="noopener" download="download"';
    } elseif(get_sub_field('button_type') == 'email') {
        $buttonLink =  'mailto:' . get_sub_field('button_link');
     } elseif(get_sub_field('button_type') == 'form') {
        $buttonLink = get_sub_field('button_link');
        $buttonAttr  = 'data-lity=true';
    } elseif(get_sub_field('button_type') == 'phone') {
        $buttonLink =  'tel:' . get_sub_field('button_link');
    } elseif(get_sub_field('button_type') == 'page') {
        $buttonLink = get_permalink(get_sub_field('page_link'));
        $buttonAttr  = '';
    } else {
        $buttonLink =  get_sub_field('button_link');
        $buttonAttr  = (get_sub_field('button_type') == 'external') ? 'target="_blank" rel="noopener"' : '';
    }
?>
<?php if($buttonText): ?>
    <a href="<?php echo $buttonLink; ?>" class="button <?php echo $buttonClass; ?>" <?php echo $buttonAttr; ?>>
        <span class="is-on-top"><?php echo $buttonText; ?></span>
    </a>
<?php endif; ?>
