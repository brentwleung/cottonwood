<article <?php post_class('search-article'); ?>>
    <h2 class="search-entry-title">
        <a href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
        </a>
    </h2>
    <div class="entry-summary">
        <?php if( get_post_type() === 'page' ) : ?>
            <?php while ( have_rows('introductory_content') ) : the_row(); ?>
                <?php if(get_sub_field( 'page_intro_content' )) : ?>
                    <p class="main-contant">
                        <?php the_sub_field( 'page_intro_content' ); ?>
                    </p>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php else: ?>
            <?php the_excerpt(); ?>
        <?php endif; ?>
    </div>
    <a class="button primary" href="<?php the_permalink(); ?>">
        See More
    </a>
</article>
