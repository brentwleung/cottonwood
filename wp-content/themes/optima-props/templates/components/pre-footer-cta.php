<?php
    while ( have_rows('pre_footer_background_colour', 'option') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('pre_footer_heading_colour', 'option') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('pre_footer_content_colour', 'option') ) : the_row();
        $contentColour = get_sub_field('global_theme_colours');
    endwhile;
?>
<section class="pre-footer-cta" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-centre">
            <div class="d-none d-md-block col-md-5 col-xl-4 pre-footer-bg-img background-center image"></div>
            <div class="col-11 col-sm-10 col-md-7 col-xl-7 content">
                <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>"><?php the_field( 'pre_footer_heading', 'option' ); ?></h2>
                <p style="color: <?php echo $contentColour; ?>"><?php the_field( 'pre_footer_content', 'option' ); ?></p>
                <?php while ( have_rows('pre_footer_button', 'option') ) : the_row(); ?>
                    <?php get_template_part('templates/button'); ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>

