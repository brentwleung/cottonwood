<ul class="contact-details-global-list">
    <li class="address">
        <span>A</span>
        <?php the_field( 'contact_address', 'option' ); ?>
    </li>
    <li class="phone">
        <span>T</span>
        <a href="tel: <?php the_field( 'contact_phone_number', 'option' ); ?>">
            <?php the_field( 'contact_phone_number', 'option' ); ?>
        </a>
    </li>
    <li class="email">
        <span>E</span>
        <a target="_blank" href="mailto:<?php the_field( 'contact_email', 'option' ); ?>">
            <?php the_field( 'contact_email', 'option' ); ?>
        </a>
    </li>
</ul>
