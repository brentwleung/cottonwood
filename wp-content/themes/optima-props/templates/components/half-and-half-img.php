<?php
    $size = 'banner-xs';
    $leftImage = get_sub_field('left_image');
    $rightImage = get_sub_field('right_image');
    $leftImgAlt = $leftImage['alt'];
    $leftImgUrl = $leftImage['sizes'][ $size ];
    $rightImgAlt = $rightImage['alt'];
    $rightImgUrl = $rightImage['sizes'][ $size ];
?>
<section class="half-and-half-img no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 background-center left-img image-col" <?php if($leftImgAlt) {echo 'alt="' . $leftImgAlt . '"'; } ?> style="background: url(<?php echo $leftImgUrl; ?>);"></div>
            <div class="col-md-6 d-none d-md-block background-center right-img image-col" <?php if($rightImgAlt) {echo 'alt="' . $rightImgAlt . '"'; } ?> style="background: url(<?php echo $rightImgUrl; ?>);"></div>
        </div>
    </div>
</section>

