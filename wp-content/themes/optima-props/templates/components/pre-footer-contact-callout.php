<?php
    while ( have_rows('contact_bg_colour', 'option') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;
?>
<section class="pre-footer-contact-callout reduced-padding" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center justify-content-xl-start">
            <div class="col-11 col-sm-12 col-md-5 col-lg-4 offset-xl-2 heading-col">
                <h2 class="main-heading has-underline"><?php the_field( 'contact_heading', 'option' ); ?></h2>
                <?php while ( have_rows('contact_directions_button', 'option') ) : the_row(); ?>
                    <?php get_template_part('templates/button'); ?>
                <?php endwhile; ?>
            </div>
            <div class="col-11 col-sm-12 col-md-5 col-lg-4 offset-lg-1 offset-xl-0 address-col">
                <?php get_template_part('templates/components/contact-detail-list'); ?>
            </div>
        </div>
    </div>
</section>

