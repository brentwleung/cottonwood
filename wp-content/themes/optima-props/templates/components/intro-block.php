<?php
    while ( have_rows('page_intro_bg_colour') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('page_intro_heading_colour') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('page_intro_content_colour') ) : the_row();
        $contentColour = get_sub_field('global_theme_colours');
    endwhile;
?>

<section class="intro-block" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-10 col-lg-9 col-xl-8 content">
                <?php if(get_sub_field( 'page_intro_heading' )) : ?>
                    <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                        <?php the_sub_field( 'page_intro_heading' ); ?>
                    </h2>
                <?php endif; ?>
                <?php if(get_sub_field( 'page_intro_content' )) : ?>
                    <p class="main-contant" style="color: <?php echo $contentColour; ?>">
                        <?php the_sub_field( 'page_intro_content' ); ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
