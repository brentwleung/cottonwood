<?php
global $wp_query, $wp_rewrite;

$query = $wp_query;

$max = $query->max_num_pages;
if ( $max <= 1 ) return;
?>

<div class="pagination-wrapper is-relative flex-center">
    <?php
        $pages = '';
        $current = max( 1, get_query_var( 'paged' ) );
        $total = 1;
        $pagis = paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ))),
            'format'       => '',
            'add_args'     => false,
            'current'      => $current,
            'total'        => $max,
            'prev_text'    => '&larr;',
            'next_text'    => '&rarr;',
            'type'         => 'array',
            'end_size'     => 2,
            'mid_size'     => 2,
            'prev_next'    => false,
        )  );
    ?>

    <div class="pagi-group-btn prev">
        <?php if($current == 1) : ?>
            <p class="link-disabled">PREVIOUS</p>
        <?php else: ?>
            <?php echo get_previous_posts_link('Prev'); ?>
        <?php endif; ?>
    </div>

    <div class="pages">
        <?php echo join($pagis);?>
    </div>

    <div class="pagi-group-btn next">
        <?php if ($current == $max): ?>
            <p class="link-disabled">NEXT</p>
        <?php else: ?>
            <?php echo get_next_posts_link('NEXT', $max); ?>
        <?php endif; ?>
    </div>
</div>
