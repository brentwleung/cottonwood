<?php
    while ( have_rows('callout_background_colour') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('callout_heading_colour') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('callout_call_to_action_colour') ) : the_row();
        $singleLineCTA = get_sub_field('global_theme_colours');
    endwhile;

    $image = get_sub_field('callout_image');
    $alt = $image['alt'];
    $size = 'thumbnail';
    $url = $image['sizes'][ $size ];
?>

<section class="content-callout" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-7 col-md-2 img-col">
                <img class="img-fluid img-circle" src="<?php echo $url; ?>" <?php if($alt) { echo 'alt="' . $alt .'"'; } ?>>
            </div>
            <div class="col-11 col-md-8 col-lg-7 col-xl-6 content">
                <?php if(get_sub_field( 'callout_heading' )) : ?>
                    <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                        <?php the_sub_field( 'callout_heading' ); ?>
                    </h2>
                <?php endif; ?>
                <?php if(get_sub_field( 'callout_content' )) : ?>
                    <?php the_sub_field( 'callout_content' ); ?>
                <?php endif; ?>
                <?php if(get_sub_field( 'callout_call_to_action_line' )) : ?>
                    <p class="main-heading" style="color: <?php echo $singleLineCTA; ?>">
                        <?php the_sub_field( 'callout_call_to_action_line' ); ?>
                    </p>
                <?php endif; ?>
                <?php while ( have_rows('callout_btn') ) : the_row(); ?>
                    <?php get_template_part('templates/button'); ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>

