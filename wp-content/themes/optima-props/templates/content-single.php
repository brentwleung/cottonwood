<?php while (have_posts()) : the_post(); ?>
    <article class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-10 col-lg-9 col-xl-8 single-content">
                <header>
                    <h1 class="h2 main-heading has-underline">
                        <?php the_title(); ?>
                    </h1>
                    <?php if (has_post_thumbnail()): ?>
                        <div class="feat-img background-center" style="background-image: url(<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(), 'banner-md') ?>);"></div>
                    <?php endif; ?>
                </header>
                <div class="entry-content">
                  <?php the_content(); ?>
                </div>
                <?php $images = get_field('post_gallery'); ?>
                <?php if($images): ?>
                    <ul class="single-gallery d-flex flex-wrap">
                        <?php foreach( $images as $image ): ?>
                            <li class="is-relative background-center" style="background: url(<?php echo $image['sizes']['banner-xs']; ?>)">
                                <a class="is-absolute" href="<?php echo $image['url']; ?>">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </article>
<?php endwhile; ?>
