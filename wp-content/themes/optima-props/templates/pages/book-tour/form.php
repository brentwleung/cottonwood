<section class="tour-form no-padding-top">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-10 col-lg-9 col-xl-8 form">
                <?php gravity_form( 3, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex = 3, $echo = true ); ?>
            </div>
        </div>
    </div>
</section>

