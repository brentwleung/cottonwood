<div class="blog-categories d-flex">
    <p>Filter News:</p>
    <div class="cat-wrap flex-center is-relative">
        <button class="mobile-cat-nav d-flex">
            <?php if(is_home()) {
                echo 'Categories';
            } else {
                single_cat_title();
            } ?>
            <span class="d-block is-relative chevron-down" aria-hidden="true"></span>
        </button>
        <nav class="blog-cat-menu">
            <ul id="menu-categories" class="blog-cat-list">
                <li class="cat-item <?php echo is_home()? 'active' : ''; ?> ">
                    <a href="<?php echo get_permalink(get_option('page_for_posts')); ?>">
                        All Posts
                    </a>
                </li>
                <?php foreach (get_categories() as $category): ?>
                    <?php $current = (is_category() && get_queried_object_id() == $category->term_id)? 'active' : ''; ?>
                    <li class="cat-item <?php echo $current; ?>">
                        <a href="<?php echo get_term_link($category); ?>"><?php echo $category->name; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>
</div>
