<?php while ( have_rows('jobs_heading_colour') ) : the_row();
    $headingColour = get_sub_field('global_theme_colours');
endwhile; ?>
<section class="careers">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-10 col-lg-9 col-xl-8 jobs-col">
                <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>"><?php the_field( 'jobs_heading' ); ?></h2>
                <?php if( have_rows('current_positions') ): ?>
                    <ul id="careers-accordion" class="accordion">
                        <?php while ( have_rows('current_positions') ) : the_row(); ?>
                        <li class="item">
                            <a class="career-trigger d-flex">
                                <span><?php the_sub_field( 'job_name' ); ?></span>
                                <div class="chevron-down"></div>
                            </a>
                            <div class="content-wrap custom-bullet-lists">
                                <?php the_sub_field( 'job_details' ); ?>
                            </div>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                <?php else: ?>
                    <p><?php the_field( 'no_current_job_openings_message' ); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

