<?php if(get_field( 'gallery_imgs' )): ?>
<?php
    while ( have_rows('gallery_heading_colour') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;

    $images = get_field('gallery_imgs');
?>

<section class="amenities-gallery">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-10 col-lg-9 col-xl-8 content">
                <?php if(get_field('gallery_heading')) : ?>
                    <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                        <?php the_field( 'gallery_heading' ); ?>
                    </h2>
                <?php endif; ?>
                <ul class="gallery d-flex flex-wrap">
                    <?php foreach( $images as $image ): ?>
                        <li class="is-relative background-center" style="background: url(<?php echo $image['sizes']['banner-xs']; ?>)">
                            <a class="is-absolute" href="<?php echo $image['url']; ?>">
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
