<?php
    while ( have_rows('amenities_heading_colour') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;
?>
<section class="amenities-main no-padding-bottom">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-10 col-xl-8">
                <h2 class="heading has-underline" style="color: <?php echo $headingColour; ?>"><?php the_field( 'amenities_heading' ); ?></h2>
                <ul class="amenities-list check-list">
                    <?php while ( have_rows('suite_amenities_list') ) : the_row(); ?>
                        <li class="is-relative"><?php the_sub_field( 'amenity' ); ?></li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

