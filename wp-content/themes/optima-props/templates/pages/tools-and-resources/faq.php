<?php while ( have_rows('faq_heading_colour') ) : the_row();
    $headingColour = get_sub_field('global_theme_colours');
endwhile; ?>

<?php if( have_rows('faq') ): ?>
    <section class="faq">
        <div class="container">
            <div class="row row-center">
                <div class="col-11 col-md-10 col-xl-8 class">
                    <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>"><?php the_field( 'faq_heading' ); ?></h2>
                    <ul id="faq-accordion" class="accordion">
                        <?php while ( have_rows('faq') ) : the_row(); ?>
                        <li class="item">
                            <a class="faq-trigger d-flex">
                                <span><?php the_sub_field( 'question' ); ?></span>
                                <div class="chevron-down"></div>
                            </a>
                            <div class="content-wrap">
                                <?php the_sub_field( 'answer' ); ?>
                            </div>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
