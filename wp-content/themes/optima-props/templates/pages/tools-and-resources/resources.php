<?php while ( have_rows('tools_and_resources') ) : the_row();
    while ( have_rows('background_colour') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('heading_colour') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('content_font_colour') ) : the_row();
        $contentColour = get_sub_field('global_theme_colours');
    endwhile;
    $image = get_sub_field('image');
    $size = 'banner-xs';
    $url = $image['sizes'][ $size ];
?>

    <section class="tools-and-resources" style="background: <?php echo $backgroundColour; ?>">
        <div class="container-fluid">
            <div class="row row-center">
                <div class="col-11 col-sm-12 col-md-4 col-xl-2 background-center image" style="background-image: url(<?php echo $url; ?>)"></div>
                <div class="col-11 col-sm-12 col-md-6 col-xl-4 content">
                    <?php if(get_sub_field( 'heading' )) : ?>
                        <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                            <?php the_sub_field( 'heading' ); ?>
                        </h2>
                    <?php endif; ?>
                    <?php if(get_sub_field( 'content' )) : ?>
                        <p class="main-heading" style="color: <?php echo $contentColour; ?>">
                            <?php the_sub_field( 'content' ); ?>
                        </p>
                    <?php endif; ?>
                    <?php while ( have_rows('button') ) : the_row(); ?>
                        <?php get_template_part('templates/button'); ?>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>
