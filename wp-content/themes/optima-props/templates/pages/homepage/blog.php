<?php
    while ( have_rows('blog_background_colour') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('blog_font_colours') ) : the_row();
        $fontColour = get_sub_field('global_theme_colours');
    endwhile;
?>

<section class="hp-blog" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-10 col-xl-8 content">
                <h2 class="main-heading has-underline" style="color: <?php echo $fontColour; ?>">
                    <?php the_field( 'blog_heading' ); ?>
                </h2>
                <ul class="cat-list d-md-flex">
                    <?php foreach (get_categories() as $category): ?>
                        <?php
                            $hero = get_field('category_image', $category);
                            $size = 'banner-xs';
                            $image = wp_get_attachment_image_src($hero, $size);
                            $url= $image[0];
                        ?>
                        <li class="cat has-absolute-after">
                            <a class="d-block background-center is-relative cat-link" href="<?php echo get_term_link($category); ?>" style="background-image: url(<?php echo $url; ?>)">
                                <h3 class="h2 cat-title no-margin absolute-center is-on-top" style="color: <?php echo $fontColour; ?>"><?php echo $category->name; ?></h3>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>
