<?php while ( have_rows('faq_heading_colour') ) : the_row();
    $headingColour = get_sub_field('global_theme_colours');
endwhile;

$image = get_field('faq_section_image');
$alt = $image['alt'];
$size = 'thumbnail';
$url = $image['sizes'][ $size ];

?>

<?php if( have_rows('faq') ): ?>
    <section class="faq hp-faq">
        <div class="container-fluid">
            <div class="row row-center">
                <div class="col-7 col-md-2 img-col">
                    <img class="img-fluid img-circle" src="<?php echo $url; ?>" <?php if($alt) { echo 'alt="' . $alt .'"'; } ?>>
                </div>
                <div class="col-11 col-md-8 col-lg-7 col-xl-6 class">
                    <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>"><?php the_field( 'faq_heading' ); ?></h2>
                    <ul id="faq-accordion" class="accordion">
                        <?php while ( have_rows('faq') ) : the_row(); ?>
                        <li class="item">
                            <a class="faq-trigger d-flex">
                                <span><?php the_sub_field( 'question' ); ?></span>
                                <div class="chevron-down"></div>
                            </a>
                            <div class="content-wrap">
                                <?php the_sub_field( 'answer' ); ?>
                            </div>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
