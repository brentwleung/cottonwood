<?php
    while ( have_rows('our_lifestyle_background_colour') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('our_lifestyle_font_colours') ) : the_row();
        $fontColour = get_sub_field('global_theme_colours');
    endwhile;
?>

<section class="hp-lifestyle" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-10 col-xl-8 content">
                <h2 class="main-heading has-underline" style="color: <?php echo $fontColour; ?>">
                    <?php the_field( 'our_lifestyle_heading' ); ?>
                </h2>
                <ul class="d-flex flex-wrap">
                    <?php while ( have_rows('page_links') ) : the_row(); ?>
                    <?php
                        $icon = get_sub_field('icon');
                        $url = $icon['url'];
                        $alt = $icon['alt'];
                    ?>
                        <li>
                            <a class="d-block text-center" href="<?php the_sub_field( 'page_link' ); ?>">
                                <img src="<?php echo $url; ?>" <?php if($alt) { echo 'alt="' . $alt .'"'; } ?>>
                                <h5 class="title" style="color: <?php echo $fontColour; ?>">
                                    <?php the_sub_field( 'heading' ); ?>
                                </h5>
                            </a>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

