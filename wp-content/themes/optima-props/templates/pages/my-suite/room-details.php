<div class="my-suit-details">
    <div class="container-fluid">
        <div class="row row-center">
            <ul class="col-11 col-md-10 col-lg-9 col-xl-8 content">
                <?php while ( have_rows('room_details') ) : the_row(); ?>
                    <li class="d-md-flex align-items-md-center">
                        <h6 class="room-name"><?php the_sub_field( 'room_category_name' ); ?></h6>
                        <p class="room-count"><?php the_sub_field( 'room_count' ); ?></p>
                        <p class="size"><?php the_sub_field( 'square_footage' ); ?></p>
                        <a class="button secondary" href="<?php the_sub_field( 'floorplan' ); ?>" target="blank">FLOOR PLAN</a>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>
</div>

