<section class="my-suite-amenities">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-md-10 col-lg-9 col-xl-8">
                <div class="row">
                    <div class="col-12 col-sm-6 left-col">
                        <h3 class="heading block-heading"><?php the_field( 'suite_amenities_heading' ); ?></h3>
                        <ul class="amenities check-list">
                            <?php while ( have_rows('suite_amenities_list') ) : the_row(); ?>
                                <li class="is-relative"><?php the_sub_field( 'amenity' ); ?></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 right-col">
                        <h3 class="heading block-heading"><?php the_field( 'common_room__amenities_heading' ); ?></h3>
                        <ul class="amenities check-list">
                            <?php while ( have_rows('common_room_amenities_list') ) : the_row(); ?>
                                <li class="is-relative"><?php the_sub_field( 'amenity' ); ?></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

