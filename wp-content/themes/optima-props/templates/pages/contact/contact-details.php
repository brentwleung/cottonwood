 <?php
    $location = get_field('google_map');
    $backgroundImage = get_field('contact_image');
    $size = 'banner-xs';
    $url = $backgroundImage['sizes'][ $size ];
    while ( have_rows('contact_background_colour') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('heading_colour') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;
?>
<section class="contact-intro no-padding" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-6 col-lg-5 details">
                <div class="bg-img is-relative background-center" style="background-image: url(<?php echo $url; ?>);"></div>
                <div class="content-wrap">
                    <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                        <?php the_field( 'contact_heading' ); ?>
                    </h2>
                    <?php get_template_part('templates/components/contact-detail-list'); ?>
                    <?php while ( have_rows('contact_directions_button', 'option') ) : the_row(); ?>
                        <?php get_template_part('templates/button'); ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="google-map col-12 col-md-6 col-lg-7 map" id="google-map"
                data-marker="<?php echo wp_get_attachment_image_url(get_field('google_marker')); ?>"
                data-url="<?php echo $location['address']; ?>"
                data-lng="<?php echo $location['lng']; ?>"
                data-lat="<?php echo $location['lat']; ?>"
                data-style="<?php echo esc_attr(get_field('google_map_style')); ?>"
            >
            </div>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdxCz5FkkqJv6e3g_p1w1jT6SzCo_JN00"></script>
        </div>
    </div>
</section>


