<section class="contact-form">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-10 col-md-9 col-xl-8 form-col">
                <h2 class="main-heading has-underline"><?php the_field( 'form_heading' ); ?></h2>
                <?php the_field( 'form_content' ); ?>
                <?php gravity_form( 5, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex = 33, $echo = true ); ?>
            </div>
        </div>
    </div>
</section>

