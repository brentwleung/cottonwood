<article class="loop-article d-md-flex">
    <div class="background-center bg-img" style="background-image: url(<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(), 'banner_xs') ?>);"></div>
    <div class="content">
        <header>
            <h2 class="entry-title has-underline">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h2>
        </header>
        <div class="entry-summary">
            <?php excerpt(20); ?>
            <a class="button tertiary text-uppercase" href="<?php the_permalink(); ?>">
                Read Story
            </a>
        </div>
    </div>
</article>
