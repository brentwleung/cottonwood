<?php
    while ( have_rows('search_bg_colour', 'option') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('search_heading_colour', 'option') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;
?>

<section class="search-results-pg"  style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-9 col-xl-8">
                <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                    <?php the_field( 'search_heading', 'option' ); ?>
                </h2>
                <h5 class="search-heading text-upp">Search</h5>
                <form class="global-search-form is-relative d-md-flex" role="search" method="GET" action="<?php echo home_url('/'); ?>">
                    <input id="global-search" class="search-bar" type="serach" placeholder='SEARCH' name="s" value="<?php echo get_search_query(); ?>">
                    <button class="button primary filter-submit" type="submit">Search</button>
                </form>

                <p class="total-results">
                    <?php echo $wp_query->found_posts.' Results'; ?>
                    <?php if (!have_posts()) : ?>
                        |&nbsp;<?php the_field( 'no_search_results_message', 'option' ); ?>
                    <?php endif; ?>
                </p>

                <?php while (have_posts()) : the_post(); ?>
                  <?php get_template_part('templates/content', 'search'); ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
