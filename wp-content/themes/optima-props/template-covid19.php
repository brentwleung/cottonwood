<?php
/**
 * Template Name: Covid-19
 */
?>

<?php
    $covidArgs = array (
        'post_type' => 'covid',
        'posts_per_page' => -1
    );

    $covidUpdates = new WP_Query( $covidArgs );
?>

<?php
    while ( have_rows('page_heading_covid_colour') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;
?>

<section class="blog-pg">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-10 col-lg-9 col-xl-9">
                <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                    <?php the_field( 'page_heading_covid' ); ?>
                </h2>
                <?php while ( $covidUpdates->have_posts() ): $covidUpdates->the_post(); ?>
                    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
