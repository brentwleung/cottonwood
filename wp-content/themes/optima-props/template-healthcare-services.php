<?php
/**
 * Template Name: Healthcare Services & Dining
 */
?>

<?php while ( have_rows('introductory_content') ) : the_row();
    get_template_part( 'templates/components/intro-block');
endwhile; ?>

<?php get_template_part('templates/pages/healthcare/service-list'); ?>

<?php get_template_part('templates/pages/amenities/gallery'); ?>
