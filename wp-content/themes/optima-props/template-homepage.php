<?php
/**
 * Template Name: Homepage
 */
?>

<?php while ( have_rows('introductory_content') ) : the_row();
    get_template_part( 'templates/components/intro-block');
endwhile; ?>

<?php while ( have_rows('management_team') ) : the_row();
    get_template_part( 'templates/components/content-callout');
endwhile; ?>

<?php get_template_part('templates/pages/homepage/faq'); ?>

<?php while ( have_rows('why_us') ) : the_row();
    get_template_part( 'templates/components/content-callout');
endwhile; ?>

<?php get_template_part( 'templates/pages/homepage/lifestyle'); ?>

<?php get_template_part( 'templates/pages/homepage/blog'); ?>
