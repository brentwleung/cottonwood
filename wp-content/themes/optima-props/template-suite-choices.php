<?php
/**
 * Template Name: Your suite
 */
?>

<?php while ( have_rows('introductory_content') ) : the_row();
    get_template_part( 'templates/components/intro-block');
endwhile; ?>

<?php while ( have_rows('intro_gallery') ) : the_row();
    get_template_part( 'templates/components/half-and-half-img');
endwhile; ?>

<div class="suite-amenities-wrap">
    <?php while ( have_rows('amenities_intro') ) : the_row();
        get_template_part( 'templates/components/intro-block');
    endwhile; ?>

    <?php get_template_part('templates/pages/my-suite/amenities'); ?>

    <?php get_template_part('templates/pages/my-suite/room-details'); ?>
</div>

<?php get_template_part('templates/components/feature-image'); ?>
