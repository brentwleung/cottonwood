<?php
/**
 * Template Name: Contact Us Page
 */
?>

<?php get_template_part('templates/pages/contact/contact-details'); ?>

<?php get_template_part('templates/pages/contact/form'); ?>
