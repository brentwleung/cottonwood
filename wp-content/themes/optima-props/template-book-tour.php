<?php
/**
 * Template Name: Book a Tour
 */
?>

<?php while ( have_rows('introductory_content') ) : the_row();
    get_template_part( 'templates/components/intro-block');
endwhile; ?>

<?php get_template_part( 'templates/pages/book-tour/form'); ?>

<?php get_template_part( 'templates/components/pre-footer-contact-callout'); ?>
