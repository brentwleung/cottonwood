<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <div class="theme-wrap">
      <!--[if IE]>
        <div class="alert alert-warning">
          <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
        </div>
      <![endif]-->
      <?php
          do_action('get_header');
          get_template_part('templates/header');
      ?>
      <div class="container-fluid" role="document">
          <div class="content row">
              <main class="main">
                <?php if(!is_404() && !is_search() && !is_single() && !is_page_template(array('template-contact.php', 'template-covid19.php', 'template-form-received.php'))) {
                  get_template_part('templates/banner');
                } elseif(is_single()) {
                  get_template_part('templates/banner-single');
                }?>
                <?php include Wrapper\template_path(); ?>

                <?php if(!is_front_page() && !is_search() && !is_404() && !is_page_template( array('template-careers.php', 'template-form-received.php', 'template-contact.php', 'template-book-tour.php'))) {
                  get_template_part('templates/components/pre-footer-cta');
                } ?>
                <?php if(!is_page_template(array('template-book-tour.php', 'template-contact.php'))) : ?>
                  <div class="sticky-book-btn d-xl-none">
                    <?php while ( have_rows('header_btn', 'option') ) : the_row(); ?>
                        <?php get_template_part('templates/button'); ?>
                    <?php endwhile; ?>
                  </div>
                <?php endif; ?>
              </main><!-- /.main -->
          </div><!-- /.content -->
      </div><!-- /.wrap -->
      <?php
          do_action('get_footer');
          get_template_part('templates/footer');
          wp_footer();
      ?>
    </div>
  </body>
</html>
