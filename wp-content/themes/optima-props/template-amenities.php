<?php
/**
 * Template Name: Amenities
 */
?>

<?php while ( have_rows('introductory_content') ) : the_row();
    get_template_part( 'templates/components/intro-block');
endwhile; ?>

<?php while ( have_rows('intro_gallery') ) : the_row();
    get_template_part( 'templates/components/half-and-half-img');
endwhile; ?>

<?php get_template_part('templates/pages/amenities/amenities'); ?>

<?php get_template_part('templates/components/feature-image'); ?>

<?php get_template_part('templates/pages/amenities/gallery'); ?>
