<?php
/**
 * Template Name: Form Received
 */
?>

<section class="form-received-pg">
    <div class="container">
        <div class="row row-center">
            <div class="col-11 col-sm-9 col-md-7 d-md-flex form-msg">
                <img src="<?php the_field('sent_image'); ?>">
                <div class="content">
                    <h1 class="h2 title has-underline"><?php the_field( 'heading' ); ?></h1>
                    <?php the_field( 'content' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>

