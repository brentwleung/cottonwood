<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);



// Add the top-level parent page to the body classes
add_filter( 'body_class', function( $classes ) {
    global $post;
    $parents = get_post_ancestors( $post->ID );
    $id = ($parents) ? $parents[count($parents)-1]: $post->ID;
    $parent = get_post( $id );
    $class = $parent->post_name;
    return array_merge( $classes, array( $class ) );
} );



// excerpt(20); OR:
// excerpt('acf=blah&id=3'); OR:
// excerpt(array('acf' => 'blah', 'id' => 3));
function excerpt($args = []) {
    // quick cheat
    if (is_int($args)) {
        $args = [
            'limit' => $args,
        ];
    }

    $args = wp_parse_args($args, [
    'string'       => '',
    'id'           => null,
    'more'         => '...',
    'limit'        => 15,
    'acf'          => '',
    'echo'         => true,
    'autop'        => true,
    'true_excerpt' => true,
    ]);

    $has_excerpt = false;

    // is there a acf set?
    if (! $args['string'] && $args['acf']) {
        // acf takes care of the id if it's null already, alternitively you can pass something like, "category_4", "people-types_3", "options"
        $args['string'] = get_field($args['acf'], $args['id']);
    }

    // if no acf, is a id being passed? if no, user the current post
    if (! $args['string'] && ! $args['id']) {
        global $post;
        $args['id'] = isset($post->ID) ? $post->ID : null;
    }

    // now we use the id to fetch the excerpt
    if (! $args['string'] && $args['id']) {
        $the_post = get_post($args['id']);

        if ($the_post) {
            if ($the_post->post_excerpt) {
                $has_excerpt = $args['true_excerpt'] ? true : false;
                $args['string'] = $the_post->post_excerpt;
            } else {
                $args['string'] = apply_filters('the_excerpt', $the_post->post_content); // get_the_excerpt();
            }
        }
    }

    // trim the conent only if true_excerpt is not set to true
    if (! $has_excerpt) {
        $args['string'] = strip_shortcodes($args['string']);
        $args['string'] = wp_trim_words($args['string'], $args['limit'], $args['more']);
    }

    // check if it needs to warp with p tag
    if ($args['autop']) {
        $args['string'] = wpautop($args['string']);
    }

    if ($args['echo']) {
        echo $args['string'];
    }

    // last, return the result (doesn't matter if it's been echoed)
    return $args['string'];
}



/*
 |--------------------------------------------------------------------------
 | Terms Helper
 |--------------------------------------------------------------------------
*/
function ce_get_terms($taxonomy, $args = []) {
    $taxonomy = get_taxonomy($taxonomy);

    $args = wp_parse_args($args, [
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'meta_query' => [
            'relation' => 'OR',
            [
                'key' => $taxonomy->object_type[0] . '_order',
                'compare' => 'NOT EXISTS'
            ],
            [
                'key' => $taxonomy->object_type[0] . '_order',
                'value' => 0,
                'compare' => '>='
            ]
        ]
    ]);

    return get_terms($taxonomy->name, $args)? : [];
}

function ce_get_the_terms($taxonomy, $args = []) {
    return ce_get_terms($taxonomy, wp_parse_args($args, [
        'object_ids' => get_the_ID()
    ]));
}

function ce_the_primary_term($taxonomy, $post_id = null, $args = []) {
    if (! $post_id) $post_id = get_the_ID();

    if (class_exists('WPSEO_Primary_Term')) {
        $wpseo_primary_term = new WPSEO_Primary_Term($taxonomy, $post_id);
        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();

        if ($wpseo_primary_term && $term = get_term($wpseo_primary_term)) {
            return $term;
        }
    }

    $terms = ce_get_the_terms($taxonomy, $post_id)? : [];

    return $terms? array_shift($terms) : false;
}

function ce_term_link($term) {
    echo ce_get_term_link($term);
}

function ce_get_term_link($term) {
    return sprintf('<a href="%s">%s</a>', get_term_link($term), $term->name);
}


//----------------------------------------------------------------
// Next post
//----------------------------------------------------------------
function cpt_next_link($post_type, $singular_name) {
    $next_args = [
        'post_type' => $post_type,
        'posts_per_page' => -1
    ];

    $cpt_post_list = [];

    $next = new WP_Query($next_args);
    while($next->have_posts()) {
        $next->the_post();
        $cpt_post_list[] = get_the_ID();
    }
    wp_reset_query();

    $current_post = intval( array_search(get_the_ID(), $cpt_post_list) );

    if( $next_post = $cpt_post_list[$current_post + 1]) {
        echo '<a href="'.get_permalink($next_post).'" class="nav-link next">Next Page <i class="fa fa-angle-right"></i></a>';
    }
}


//----------------------------------------------------------------
// Previous post
//----------------------------------------------------------------
function cpt_prev_link($post_type, $singular_name) {
    $prev_args = [
        'post_type' => $post_type,
        'posts_per_page' => -1
    ];

    $cpt_post_list = [];

    $prev = new WP_Query($prev_args);
    while($prev->have_posts()) {
        $prev->the_post();
        $cpt_post_list[] = get_the_ID();
    }
    wp_reset_query();

    $current_post = intval( array_search(get_the_ID(), $cpt_post_list) );

    if( $prev_post = $cpt_post_list[$current_post - 1]) {
        echo '<a href="'.get_permalink($prev_post).'" class="nav-link next"><i class="fa fa-angle-left"></i> Prev Page</a>';
    }
 }
