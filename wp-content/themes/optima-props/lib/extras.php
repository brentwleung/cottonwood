<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Set up custom theme options pages for ACF
 */
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'General Site Options',
    'menu_title'  => 'Site Options',
    'menu_slug'   => 'site-options',
    'capability'  => 'edit_posts',
    'position'     => '62.1',
    'redirect'    => true,
    'capability'  => 'manage_options',
  ));

    acf_add_options_sub_page(array(
      'page_title'  => 'General Options',
      'menu_title'  => 'General',
      'parent_slug' => 'site-options',
  ));

  acf_add_options_sub_page(array(
      'page_title'  => 'Header Options',
      'menu_title'  => 'Header',
      'parent_slug' => 'site-options',
  ));

  acf_add_options_sub_page(array(
      'page_title'  => 'Footer Options',
      'menu_title'  => 'Footer',
      'parent_slug' => 'site-options',
  ));

  acf_add_options_sub_page(array(
      'page_title'  => '404 Error Page',
      'menu_title'  => '404 Error',
      'parent_slug' => 'site-options',
  ));
}


/**
 * Make Gravity Forms Field a Button
 */
function gf_make_submit_input_into_a_button_element($button_input, $form) {

  //save attribute string to $button_match[1]
  preg_match("/<input([^\/>]*)(\s\/)*>/", $button_input, $button_match);

  //remove value attribute
  $button_atts = str_replace("value='".$form['button']['text']."' ", "", $button_match[1]);

  return '<button class="button primary" '.$button_atts.'>'.$form['button']['text'].'</button>';
}
add_filter('gform_submit_button', __NAMESPACE__ . '\\gf_make_submit_input_into_a_button_element', 10, 2);


/**
 * When submitted, return to Form, no matter where it is on the page
 */

add_filter( 'gform_confirmation_anchor', '__return_true' );


/**
 * Force Gravity Forms to init scripts in the footer and ensure that the DOM is loaded before scripts are executed
 */
add_filter( 'gform_init_scripts_footer', '__return_true' );

add_filter( 'gform_cdata_open', __NAMESPACE__ . '\\wrap_gform_cdata_open', 1 );

function wrap_gform_cdata_open( $content = '' ) {
  if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
    return $content;
  }
  $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
    return $content;
}

add_filter( 'gform_cdata_close', __NAMESPACE__ . '\\wrap_gform_cdata_close', 99 );

function wrap_gform_cdata_close( $content = '' ) {
  if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
    return $content;
  }

    $content = ' }, false );';

    return $content;
}


/**
 * Wordpress Admin Logo and Link changes
 */
function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', __NAMESPACE__ . '\\my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', __NAMESPACE__ . '\\my_login_logo_url_title' );

/**
* Add company logo at the login screen
*/
function custom_login_logo() {
  echo '<style>
    .login h1 a {
      background-image:url('.get_bloginfo('stylesheet_directory').'/dist/images/cottonwoo_logo.svg) !important;
    background-size: 290px 200px !important;
    width: 290px !important;
    height: 130px;
  }
    </style>';
}
add_action('login_head', __NAMESPACE__ . '\\custom_login_logo');

/**
* custom color swatches in Wysiwyg
*/

function my_mce4_options($init) {

    $custom_colors = '
        "5c2832", "Primary (Maroon)",
        "d07945", "Secondary (Orange)",
        "858b72", " Tertiary (Green)",
        "fff", "White",
        "f2f0f0;", "Grey",
        "000", "Black",
        "303338", "Main Font Colour",
    ';

    // build color grid default+custom colors
    $init['textcolor_map'] = '['.$custom_colors.']';

    // change the number of rows in the grid if the number of colors changes
    // 8 swatches per row
    $init['textcolor_rows'] = 1;

    return $init;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\my_mce4_options');

/**
* Google Maps
*/


function my_acf_init() {

  acf_update_setting('google_api_key', 'AIzaSyBdxCz5FkkqJv6e3g_p1w1jT6SzCo_JN00');
}

add_action('acf/init', __NAMESPACE__ . '\\my_acf_init');

