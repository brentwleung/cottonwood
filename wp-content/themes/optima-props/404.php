<?php
    while ( have_rows('bg_colour_404', 'option') ) : the_row();
        $backgroundColour = get_sub_field('global_theme_colours');
    endwhile;

    while ( have_rows('message_colour_404', 'option') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;
?>
<section class="page-404" style="background: <?php echo $backgroundColour; ?>">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-10 col-lg-9 col-xl-8 flex-center content-wrap">
                <div class="content d-md-flex">
                    <img src="<?php the_field('icon_image_404', 'option'); ?>" alt="404 icon">
                    <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                        <?php the_field( 'message_404', 'option' ); ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>

