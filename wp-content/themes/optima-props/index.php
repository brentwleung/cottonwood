<?php
    while ( have_rows('page_heading_blog_colour', 'option') ) : the_row();
        $headingColour = get_sub_field('global_theme_colours');
    endwhile;
?>

<section class="blog-pg">
    <div class="container-fluid">
        <div class="row row-center">
            <div class="col-11 col-sm-12 col-md-10 col-lg-9 col-xl-9">
                <h2 class="main-heading has-underline" style="color: <?php echo $headingColour; ?>">
                    <?php the_field( 'page_heading_blog', 'option' ); ?>
                </h2>

                <?php get_template_part( 'templates/pages/blog/blog-categories' ); ?>

                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                <?php endwhile; ?>

                <?php get_template_part('templates/components/pagination'); ?>

            </div>
        </div>
    </div>
</section>
