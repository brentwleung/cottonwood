<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cottonwood');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[91U4#Kuw@#N*#mkh{?Fp&roZYoG<!$%cjW]e6p1dm0BOZ8*Y(/o;K!I*rR}a8FH');
define('SECURE_AUTH_KEY',  'h&%v<O=Jnw{WP!w@QlUO;/-wE]v8sqLj[!=8g3qhQL;_ `6fNJ`v7t?L&(4TrV(x');
define('LOGGED_IN_KEY',    '-HeltaJ)L8$YTDZ,]+Iuu%NWPT@]L,d-$|{3-=eCl*NnqxfghLCI>lvIIB-*ct1|');
define('NONCE_KEY',        '6OoUWwP|#w=Pjey4&wXI%lTq[i!fXh $GE;wp94pVVT39rfH|AV`9r@25Ez3u+X ');
define('AUTH_SALT',        ',y$$_<{ILh8DT8@NCa].MZ?FotciWHovT{$v9KhK2]%1sg~{5w}9JN)#)LJdNVpv');
define('SECURE_AUTH_SALT', 'k52fTtB-jBWLmjRoSl&Quve{|C;FxDo!r]W@tt:sa-K}O-.y5hFf&37D#Nfra4kz');
define('LOGGED_IN_SALT',   'XF^=3O/7|siE#[aQNQi2[(=Y+MU|^t-mj2-x$E00Ndk}C}Nnt;>l^-^Ek7HeX]cg');
define('NONCE_SALT',       '5mf1k^DE1G.<4_Xy`gBi[nHcyA+L]Lb|=8 /Z9|jd|{~:-fYD.eqqt!0QkC~8sFz');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '_u9w7F';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
